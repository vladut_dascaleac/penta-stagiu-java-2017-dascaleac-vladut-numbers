package numbers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public class PentaNumbersList {
    Collection<Integer>  list;
    
    public PentaNumbersList(){
        list = Arrays.asList(234, 678, 131, 101, 199, 346);
    }
    public int getLargestNumber(){
        return Collections.max(list);
    }
    public int getSmallestNumber(){
        return Collections.min(list);
    }
    public Collection<Integer> getPrimeNumbers(){
        Collection<Integer> primeNumbers = new ArrayList<>();
        for(int prime : list)
                if(isPrime(prime))
                    primeNumbers.add(prime);
        return primeNumbers;
    }
    public Boolean isPrime(int number){
        if(number%2==0)
            return false;
        for(int i=3;i<=Math.sqrt(number);i=i+2)
            if(number%i==0)
                return false;
        return true;
    }
    public Collection<Integer> getPalindromeNumbers(){
        Collection<Integer> palindromeNumbers = new ArrayList<>();
        for(int palindrome : list)
            if(isPalindrome(palindrome))
                palindromeNumbers.add(palindrome);
        return palindromeNumbers;
    }
    public Boolean isPalindrome(int number){
        String reverse = "";
        String stringNumber =  String.valueOf(number);
        for(int i=stringNumber.length()-1;i>=0;i--)
            reverse += stringNumber.charAt(i);
        //System.out.println("String number is :"+stringNumber);
        //System.out.println("reverse number is :"+reverse);
        if(stringNumber.equals(reverse))
            return true;
        return false;
    }
    public void showLargestNumber(){
        System.out.println("Largest number: "+getLargestNumber());
    }
    public void showSmallestNumber(){
        System.out.println("Smallest number:"+getSmallestNumber());
    }
    public void showPrimeNumbers(){
        System.out.print("Prime numbers: ");
        for(int i : getPrimeNumbers())
            System.out.print(i+", ");
        System.out.println();
    }
    public void showPalindromeNumbers(){
        System.out.print("Palindrome numbers: ");
        for(int i : getPalindromeNumbers())
            System.out.print(i+", ");
        System.out.println();
    }
    
}
