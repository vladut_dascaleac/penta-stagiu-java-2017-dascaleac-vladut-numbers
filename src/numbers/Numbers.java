package numbers;

public class Numbers {

    public static void main(String[] args) {
        PentaNumbersList pentaNumbers = new PentaNumbersList();
        pentaNumbers.showLargestNumber();
        pentaNumbers.showSmallestNumber();
        pentaNumbers.showPalindromeNumbers();
        pentaNumbers.showPrimeNumbers();
    }
    
}
